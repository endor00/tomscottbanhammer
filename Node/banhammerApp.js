// Twitch account remote control
const tmi = require('tmi.js');
const channelName = "myChannel"// set this to the channel you want to moderate
const modUsername = "myMod" // set this to the moderator user you want to remote control
const modPassword = "myPasswordOAuth" // generate an OAuth by logging into the chosen account, then go to twitchapps.com/tmi

// configure the client, then connect
var options = {
    options: {
        debug: false
    },
    connection: {
        cluster: "aws",
        reconnect: true
    },
    identity: {
        username: modUsername,
        password: modPassword
    },
    channels: [channelName]
};

var client = new tmi.client(options);
client.connect();

var isTwitchConnected = false;

client.on("disconnected", function (address, port) {
    isTwitchConnected = false;
    console.log("\n - TWITCH DISCONNECTED - \n");
});

client.on('connected', function (address, port) {
    isTwitchConnected = true;
    console.log("\n - TWITCH READY - \n");
});

// interactive console
const readline = require('readline');
const interactiveConsole = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var banTarget = ""

interactiveConsole.on('line', (input) => {
    stageUser(input);
});

// connection to Unity app
const socketIO = require("socket.io")(3000);
var socket = null;

socketIO.on("connection", function(_socket){
    socket = _socket;
    console.log("\n - UNITY READY - \n");

    socket.on("ban", function(data) {
        rawUsername = JSON.stringify(data["username"]);
        rawUsernameSliced = rawUsername.slice(1, rawUsername.length - 1);
	rawDuration = JSON.stringify(data["duration"]);
        rawDurationSliced = rawDuration.slice(1, rawDuration.length - 1);
        console.log("Attempting to ban : " + rawUsernameSliced + " for " + rawDurationSliced + " seconds...");
        ban(rawUsernameSliced, rawDurationSliced);
    });

})

// user to ban
function stageUser (usernameToStage) {
    if (socket != null)
    {
        socket.emit("stageUser", { usernameToStage });
        console.log(usernameToStage + " is at the mercy of the Banhammer");
    }
    else
    {
        console.log("Failed to stage " + usernameToStage + ". Unity socket is not connected")
    }
}

// ban event
function ban(usernameToBan, banDuration) {
    if (isTwitchConnected) {
        client.timeout(channelName, usernameToBan, banDuration, "").then(function(data) {
            console.log(usernameToBan + "  has been Banhammered!!!");
        }).catch(function(err) {
            console.log("Error  : " + err);
        });
    }
}