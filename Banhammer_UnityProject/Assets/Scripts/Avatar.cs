﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Avatar : MonoBehaviour {

	[SerializeField]
	Material glowMaterial;
	[SerializeField]
	Material sparksMaterial;
	[SerializeField]
	Material nameMaterial;
	[SerializeField]
	Light avatarLight;
	[SerializeField]
	Light nameLight;
	[SerializeField]
	GameObject destructionDebris;

	[SerializeField]
	AnimationCurve transitionInCurve;
	float transitionIn;

	[SerializeField]
	UnityEngine.UI.Text nameText;

	Color uniqueColour;

	string username;
	
	void Update()
	{
		float pulsingGlow = ((Mathf.Sin(Time.time * 2.0f) + 1.0f) * 0.5f) * 1.0f + 3.0f + ((1.0f - transitionInCurve.Evaluate(transitionIn)) * 30.0f);
		glowMaterial.SetColor("_EmissionColor", uniqueColour * pulsingGlow);
		sparksMaterial.SetColor("_EmissionColor", uniqueColour * pulsingGlow);
		nameMaterial.SetColor("_EmissionColor", uniqueColour * 1.5f);
		avatarLight.color = uniqueColour;
		nameLight.color = uniqueColour;

		if (transitionIn < 1.0f)
		{
			transitionIn += Time.deltaTime * 1.0f;
		}
		else 
		{
			transitionIn = 1.0f;
		}

		transform.localScale = transitionInCurve.Evaluate(transitionIn) * Vector3.one;	
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "BanHammerHead") // use tags to detect the right collider
		{
        	Smash(collision.contacts[0].point);
		}
    }

	public void Init(string _username)
	{
		username = _username;
		uniqueColour = Color.HSVToRGB(Random.Range(0.0f, 1.0f), 0.7f, 1.0f);
		gameObject.name = "Avatar: " + username;
		nameText.text = username.Substring(1, username.Length - 2); //remove "" around name

		transitionIn = 0.0f;
		transform.localScale = Vector3.zero;
	}

	public void Smash(Vector3 contactPos)
	{
		int powerToDuration = Mathf.RoundToInt(Managers.Stage.Banhammer.SwingPower * 1000);
		if (Managers.Debug)
		{
			Debug.Log("Smashed " + username + "!" + "   —   " + powerToDuration);
		}
		GameObject debrisObj = GameObject.Instantiate(destructionDebris, transform.position, transform.rotation);
		AvatarDebris debris = debrisObj.GetComponent<AvatarDebris>();
		debris.Init(contactPos, uniqueColour);

		Managers.Stage.TriggerBan(powerToDuration);
		Remove();
	}

	public void Remove()
	{
		GameObject.Destroy(gameObject);
	}
}
