﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour, PublicStageManager {

	[SerializeField]
	GameObject plinth;

	[SerializeField]
	GameObject avatarPrefab;

	public Banhammer Banhammer {
		get { return banhammer; }
	}
	[SerializeField]
	Banhammer banhammer;

	public Avatar CurrentAvatar {
		get { return currentAvatar; }
		set { currentAvatar = value; }
	}
	Avatar currentAvatar;

	public string CurrentUsername {
		get { return currentUsername; }
		set { currentUsername = value; }
	}
	string currentUsername = null;

	bool is_staged = false;

	public void ResetAndStage(string _username)
	{
		if (Managers.Debug)
		{
			Debug.Log("Staging : " + _username);
		}
		currentUsername = _username;

		if (currentAvatar != null){
			currentAvatar.Remove();
		}

		SpawnAvatar(_username);

		is_staged = true;
	}

	void SpawnAvatar(string _username)
	{
		currentAvatar = GameObject.Instantiate(avatarPrefab, plinth.transform.position, Quaternion.identity).GetComponent<Avatar>();
		currentAvatar.Init(_username);
	}

	public void TriggerBan(int _duration)
	{
		if (currentUsername != null && is_staged)
		{
			Managers.Node.Ban(currentUsername, _duration);
			is_staged = false;
		}
	}

}

public interface PublicStageManager {
	 void ResetAndStage(string _username);
	 void TriggerBan(int _duration);
	 string CurrentUsername { get; set; }
	 Avatar CurrentAvatar { get; set; }
	 Banhammer Banhammer { get; }
}
