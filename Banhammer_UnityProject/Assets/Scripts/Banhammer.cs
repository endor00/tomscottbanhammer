﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Banhammer : MonoBehaviour {

	[SerializeField]
	Transform gripHand;
	[SerializeField]
	Transform pivotHand;

	[SerializeField]
	Transform hammerGrip;
	[SerializeField]
	Transform hammerPivot;

	VelocitySampler headVelocitySampler;

	[SerializeField]
	float swingPower;
	public float SwingPower
	{
		get { return swingPower; }
	}

	void Awake()
	{
		headVelocitySampler = GetComponentInChildren<VelocitySampler>();
	}
	
	void Update ()
	{
		if (gripHand != null)
		{
			hammerGrip.transform.position = gripHand.transform.position;
		}

		if (pivotHand != null)
		{
			hammerPivot.transform.position = pivotHand.transform.position;
		}

		if (headVelocitySampler != null)
		{
			headVelocitySampler.Sample();
			swingPower = headVelocitySampler.Velocity;
		}
	}
}